section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax,60 ;'exit' syscall number
	syscall ; system call

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ;counter = 0
	.foreach: cmp byte[rax+rdi],0 ;сравниваем символ по индексу с нулем
	ja .increment ; если не конечц строки, то переход в .increment
    ret
	.increment: inc rax ; counter+=1
	jmp .foreach ;переход к следующему элементу строкии

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax,rax
	push rdi
	call string_length ;получаем длину строки
	pop rdi
	mov rdx,rax ;длина строки в байтах
	mov rax,1 ; 'write' syscall number
	mov rsi,rdi; string address
	mov rdi,1; stdout descriptor
	syscall ; system call
	ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
	mov rax,1 ; 'write' syscall number
	push rdi ;используем стек для передчи указателя на символ в rsi
	mov rsi,rsp ; string address
	pop rdi ; очищаем стек
	mov rdi,1 ; stdout descriptor
	mov rdx,1 ; длина строки в байтах
	syscall ; system call
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi,0xA ;помещаем в rdi нужный символ '\n'
    jmp print_char ; выводим этот символ

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
	mov r8,10 ; устанавливаем систему счисления
	mov rsi,rsp ; сохраняем начальный указатель стека
	push 0 ; загружаем в стек конец строки - '0'
	mov rax,rdi ;помещаем в rax число из rdi
	.for: 	xor rdx,rdx ; обнуляем rdx
			div r8 ;целую часть от деления помещаем в rax, остаток в rdx
			add rdx, 0x30 ; переводим цифру в ASCII
			dec rsp ; спускаемся в низ по стеку
			mov byte[rsp],dl ; кладем цифру в стек
			cmp rax,0
			ja .for ;если записано не все число, то переход в .for
			mov rdi,rsp ;помещаем в rdi адрес начала строки
			push rsi ; сохраняем значение rsi
			call print_string ; выводим число
			pop rsi
			mov rsp,rsi ; восстанавливаем стек до начального состояния
			ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
	cmp rdi,0 ; сравниваем число с 0
	jge print_uint ; если число неотрицательное, то выводим его как беззнаковое
	push rdi ; иначе сохраняем число в стек
	mov rdi,'-'
	call print_char ; выводим '-'
	pop rdi ; помещаем число в rdi
	neg rdi ; меняем знак числа (доп код)
	jmp print_uint ; выводим модуль числа

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	xor rcx,rcx ; counter =0
	.foreach: 
	mov r8b,byte[rdi+rcx] ; берем символ из первой строки
	cmp r8b,byte[rsi+rcx] ; сравниваем символы
	jne .notequal ; если не равны, то переход в .notequal
	cmp r8b,0 ; если равны, проверяем на конец строки
	je .equal ; если конец строки, переход в .equal
	inc rcx ; если не конец строки, то counter+=1
	jmp .foreach ; переход к следующему символу в строках
	
	.equal: mov rax,1 ; если равны, то вернуть 1
	ret
	.notequal: mov rax,0 ; если не равны вернуть 0
	ret
	
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ; 'read' syscall number
	xor rdi,rdi ; stdin descriptor
	push 0 ; выделяем в стеке память под считываемый символ
	mov rsi,rsp ; устанавливаем адресс чтения
	mov rdx,1 ; длина строки в байтах
	syscall ; system call
	pop rax ; помещаем символ в rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rax,rax
	xor rcx,rcx ; counter=0
	.foreach: push rcx
	push rdi
	push rsi
	call read_char ; считываем символ
	pop rsi
	pop rdi
	pop rcx
	cmp rax,0x20 ; проверяем является ли пробелом
	je .skip_spaces ; если пробел, то переход в .skip_spaces
	cmp rax,0x9 ; проверяем является ли табуляцией
	je .skip_spaces ; если табуляция, то переход в .skip_spaces
	cmp rax,0xA ; проверяем является ли переносом строки
	je .skip_spaces ; если перенос строки, то переход в .skip_spaces
	cmp rax,0 ; проверяем является ли концом строки
	je .end_of_string ; если конец, то переход в .end_of_string
	mov [rdi+rcx],rax ; если обычный символ, то записываем в ячейку по индексу
	inc rcx ; counter+=1
	cmp rcx,rsi ; проверяем на выход за пределы буфера
	jge .buffer_limit ; если лимит превышен, то переход в .buffer_limit
	jmp .foreach ; иначе переход к следующему символу
	.skip_spaces: cmp rcx,0
	je .foreach ; если слово еще не считывается, то переход к следующему символу
	jmp .end_of_string ; иначе это конец слова, переход в .end_of_string
	.buffer_limit: xor rax,rax ; возвращает 0
	ret
	.end_of_string: xor rax,rax
	mov [rdi+rcx],rax ; записываем 0 - конец строки
	mov rax,rdi ; возвращает адресс буфера	
	mov rdx,rcx ; возвращает длину слова
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rcx, rcx ; counter=0
	mov r8, 10 ; устанавливаем систему счисления
	.foreach: movzx r9,byte[rdi+rcx] ; считываем символ с расширением старших байтов (заполняются 0)
	cmp r9,0
	je .end_or_not_number ; если конец строки, то переход в .end_or_not_number
	cmp r9b,'0'
	jl .end_or_not_number ; если символ не является цифрой, то переход в .end_or_not_number
	cmp r9b,'9'
	jg .end_or_not_number ; если символ не является цифрой, то переход в .end_or_not_number
	mul r8 ; сдвигаем число в rax на один разряд влево
	sub r9b,0x30 ; получаем цифру из ее кода в ASCII
	add rax,r9 ; добавляем в младший разряд rax полученную цифру
	inc rcx ; counter+=1
	jmp .foreach ; переход к следующему символу в строке
    
	.end_or_not_number: mov rdx,rcx ; возвращает в rdx длину числа
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax ; обнуляем регистр
	xor rdx,rdx ; counter=0
	cmp byte[rdi],'-'
	je .negative ; если в начале '-', то переход в .negative
	jmp parse_uint ; иначе число неотрицательное, парсим как беззнаковое
	.negative: inc rdi ; смещаем начало строки на один символ - '-'
	call parse_uint ; парсим число
	neg rax ; меняем знак (доп код)
	inc rdx ; добавляем знак к длине числа
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax,rax
	push rdi
	push rsi
	push rdx
	call string_length ; получаем длину строки
	pop rdx
	pop rsi
	pop rdi
	cmp rax,rdx ; проверяем вмещается ли строка в буфер
	jg .buffer_limit ; если нет, то переход в .buffer_limit
	xor rcx,rcx ; counter=0
	.foreach: cmp rcx,rax ; проверяем остались ли символы в строке
	jg .end_of_string ; если нет, то переход в .end_of_string
	mov r9b, [rdi+rcx] ; помещаем символ из строки в r9
	mov [rsi+rcx], r9b ; помещаем символ из r9 в буфер
	inc rcx ; counter+=1
	jmp .foreach ; переходим на следующий символ
	.end_of_string: ; строка скопирована, возвращаем ее длину
	ret
	.buffer_limit: xor rax,rax ; строка не вместилась в буфер, возвращаем 0
	ret
